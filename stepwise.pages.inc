<?php
/**
 *
 *
 * @file
 */

/**
 * List the available workflows.
 *
 * @return string
 */
function stepwise_configuration_list() {
  $links['title'] = t('Workflow list');
  $workflows = stepwise_get_configurations();

  foreach ($workflows as $workflow) {
    $links['items'][] = stepwise_link($workflow);
  }

  return theme('item_list', $links);
}


/**
 * This form provide the multistep form for the workflows.
 *
 * @param $module
 * @param $id
 *
 * @return bool
 */
function stepwise_page_form($module, $id, $js = NULL, $step = NULL) {
  if (!module_exists($module)) {
    return array();
  }

  // At the first step we should load all of the steps.
  if (empty($form_state['workflow_info'])) {
    $workflow_infos = stepwise_get_configurations();
    if (!isset($workflow_infos[$id])) {
      return array();
    }
  }

  $form_info = array(
    'id' => 'stepwise', //form id
    'path' => "stepwise/" . $module . "/" . $id . "/display/" . ($js ? 'ajax' : 'nojs') . '/%step/step',
    'show trail' => FALSE, //show the breadcrumb / path trail for the forms
    'show back' => FALSE, //show the back button
    'show cancel' => FALSE, //show the cancel button
    'show return' => FALSE, //show the update and return button
    'finish callback' => 'stepwise_page_form_submit', //callback when finish button is called
    'next text' => t('Next'),
    'auto cache' => TRUE,
  );

  foreach ($workflow_infos[$id]->steps as $conf) {
    $form_info['order'][] = $conf['form'];
    $form_info['forms'][] = array(
      // The ctools functions aren't calling the form functions directly.
      'form id' => 'stepwise_multistepform_wrapper_calback',
      // The actual step form id.
      'current form id' => $conf['form'],
      'include' => stepwise_load_include_files($conf['url']),
    );
  }

  // Init the step variable.
  if (empty($step)) {
    $step = 0;
  }

  // Send this all off to our form. This is like drupal_get_form only wizardy.
  ctools_include('wizard');
  ctools_include('object-cache');
  $form = ctools_wizard_multistep_form($form_info, $step, $form_state);
  $output = drupal_render($form);
  return $output;
}

/**
 * This is a wrapper function for managing formbuilding during the steps.
 * This wrapper function is add because the system setting forms are usually has not any parameters,
 * that casuing the form wrapper callback function results will lost. The ctools wizard api is heavily using
 * the forms wrapper callback functions.
 *
 * @param $form
 * @param $form_state
 */
function stepwise_multistepform_wrapper_calback($form, &$form_state) {
  $step = $form_state['step'];
  $current_form_id = $form_state['form_info']['forms'][$step]['current form id'];
  // Build the given form.
  $form += drupal_build_form($current_form_id, $form_state);
  // Hide every submit buttons.
  stepwise_remove_submit_buttons($form);
  return $form;
}

/**
 * The workflow finish callback.
 *
 * @param $form
 * @param $form_state
 */
function stepwise_page_form_submit(&$form_state) {
  $form_state['redirect'] = array('admin/config/workflow/stepwise');
}

/**
 * This form collect the necessary informations for workflow creation.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function stepwise_add_form($form, &$form_state) {
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Use only lowercase alphanumeric characters, underscores (_), and hyphens (-).'),
    '#required' => TRUE,
    '#element_validate' => array('stepwise_workflow_name_validate'),
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('The workflow description.'),
    '#required' => TRUE,
  );

  $form['group'] = array(
    '#type' => 'textfield',
    '#title' => t('Group'),
    '#description' => t('The group of the workflow.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Element validate function to ensure that the workflow name is in the correct format.
 *
 * @param $element
 * @param $form_state
 */
function stepwise_workflow_name_validate($element, $form_state) {
  $modules = array();
    // Get current list of modules.
  $files = system_rebuild_module_data();
  foreach ($files as $filename => $file) {
    if (!isset($file->info['hidden'])) {
      $modules[] = $filename;
    }
  }

  // Check for duplicates.
  if (in_array($element['#value'], $modules)) {
    form_set_error($element['#name'], t('The %name module name is already taken. Please choose an other.',
      array('%name' => $element['#value'])));
  }

  // Check for illegal characters in the workflow name.
  if (preg_match('/[^0-9a-z_\-]/', $element['#value'])) {
    form_set_error($element['#name'], t('Please only use lowercase alphanumeric characters, underscores (_), and hyphens (-) for style names.'));
  }
}

/**
 * Set up the basic inforamtions.
 *
 * @param $form
 * @param $form_state
 */
function stepwise_add_form_submit($form, &$form_state) {
  $_SESSION['active_stepwise'] = $form_state['values']['name'];
  $workflow = new stdClass();
  $workflow->label = $form_state['values']['name'];
  $workflow->group = $form_state['values']['group'];
  $workflow->description = $form_state['values']['description'];
  $workflow->steps = array();
  stepwise_object_cache_set($form_state['values']['name'], $workflow);
  $form_state['redirect'] = array("stepwise/$workflow->label/edit");
}

/**
 * The workflow builder edit form.
 *
 * @param $form
 * @param $form_state
 * @param $id
 *
 * @return array
 */
function stepwise_edit_form($form, &$form_state, $id) {
  $workflow = stepwise_object_cache_get($id);
  if (isset($_GET['form']) && isset($_GET['destination'])) {
    $workflow->steps[] = array(
      'formid' => $_GET['form'],
      'url' => $_GET['destination'],
    );
  }
  stepwise_object_cache_set($id, $workflow);

  if (!empty($workflow->steps)) {
    $form['elements']['#tree'] = TRUE;
    foreach ($workflow->steps as $key => $step) {
      $form['elements'][$key]['formid'] = array(
        '#type' => 'hidden',
        '#value' => $step['formid'],
      );
      $form['elements'][$key]['url'] = array(
        '#type' => 'hidden',
        '#value' => $step['url'],
      );
      $form['elements'][$key]['label'] = array(
        '#markup' => $step['formid'],
      );
      $form['elements'][$key]['remove'] = array(
        '#markup' => l('Remove', "admin/config/workflow/stepwise/$key/remove",
          array('query' => array(drupal_get_destination()))),
      );
      $form['elements'][$key]['weight'] = array(
        '#type' => 'weight',
        '#title_display' => 'invisible',
        '#title' => t('Weight'),
        '#default_value' => $key,
        '#attributes' => array('class' => array('configuration-workflow-form-sequence')),
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Export'),
  );

  return $form;
}

/**
 * Export the active workflow.
 *
 * @param $form
 * @param $form_state
 */
function stepwise_edit_form_submit($form, &$form_state) {
  if (empty($form_state['values']['elements'])) {
    return;
  }

  // Remove the last step to avoid the duplications.
  if (isset($_GET['form']) && isset($_GET['destination'])) {
    array_pop($form_state['values']['elements']);
  }

  $cache = stepwise_object_cache_get($_SESSION['active_stepwise']);
  $workflow = $cache;
  unset($workflow->steps);

  foreach ($form_state['values']['elements'] as $step) {
    $workflow->steps[] = array(
      'form' => $step['formid'],
      'url' => $step['url'],
      'weight' => $step['weight'],
    );
  }

  $stepwiseModuleExport = new StepwiseModuleExport($workflow);
  $stepwiseModuleExport->export();

  ctools_include('object-cache');
  ctools_object_cache_clear('stepwise', $_SESSION['active_stepwise']);
  unset($_SESSION['active_stepwise']);
}
